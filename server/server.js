"use strict";

var express = require('express'); // http://expressjs.com/en/4x/api.html
var app = express();

app.get('/fecha', function(req, res){
    res.send(new Date());
    res.end();
});

app.listen(3000,function(){
    console.log('conectado');
});

app.use(express.static('public')); // http://expressjs.com/en/starter/static-files.html


var pg = require('pg'); // https://www.npmjs.com/package/pg#client-pooling
var config = require('../local-config-db.json')
var pool = new pg.Pool(config);

app.get('/instalar', function(req,res){
	pool.query("select * from preguntas order by orden")
	.then(function(result){
		var sql='CREATE TABLE encuesta ( \n'+
			result.rows.map(function(row){
				return row.variable+' '+(
					row.tipo=='numero'?'integer':'text'
				)+', \n';
			}).join('')+
			' primary key (id));'
		return pool.query(sql);
	}).then(function(result){
		console.log(result);
		res.send('<pre>OK: \n'+result.command);
		res.end();
	}).catch(function(err){
		console.log(err);
		res.send(err.message);
		res.end();
	});
});

app.get('/ingresar', function(req,res){
	pool.query("select * from preguntas order by orden")
	.then(function(result){
	var html=`<style>
.aclaracion{
  font-size: 80%
  margin-left: 30px
}
.numero{
	width: 50px
}
input{
  margin-left: 30px
}
</style>
<h1>Encuesta de cupones</h1>
<form action=/guardar method=get>
`+
			result.rows.map(function(row){
				return "<p>"+row.pregunta+"</p>\n"+
				"<input name="+row.variable+" class="+row.tipo+">\n"+
				"<p class=aclaracion>"+(row.aclaracion||'')+"</p>\n";
			}).join('')+
			"<button name=guardar>grabar</button>"
		res.send(html);
		res.end();
	}).catch(function(err){
		console.log(err);
		res.send(err.message);
		res.end();
	});
});

app.get('/guardar', function(req,res){
	pool.query("select * from preguntas order by orden")
	.then(function(result){
		var sql='INSERT INTO encuesta ('+
			result.rows.map(function(row){
				return row.variable;
			}).join(',')+
			") values ("+
			result.rows.map(function(row, posicion){
				return '$'+(posicion+1);
			}).join(',')+
			")"
		return pool.query(sql,
			result.rows.map(function(row){
				return req.query[row.variable]||null;
			})
		)
	}).then(function(result){
		res.send('ok ' + result.command);
		res.end();
	}).catch(function(err){
		console.log(err);
		res.send(err.message);
		res.end();
	});
});


app.get('/listado-ingresadas', function(req,res){
	var preguntas
	pool.query("select * from preguntas order by orden")
	.then(function(result){
		preguntas=result.rows;
		return pool.query("select * from encuesta order by id")
	}).then(function(result){
		var html=`<style>
.aclaracion{
  font-size: 80%
  margin-left: 30px
}
.numero{
	width: 50px
}
input{
  margin-left: 30px
}
td{
	border: 1px solid green
}
</style>
<h1>Listado de encuestas ingresadas</h1>
`+
			"<table><tr>"+
				preguntas.map(function(pregunta){
					return "<th title='"+pregunta.pregunta+"'>"+pregunta.variable;
				}).join('')+
			result.rows.map(function(row){
				return "<tr>"+
				preguntas.map(function(pregunta){
					return "<td>"+row[pregunta.variable];
				}).join('');
			}).join('');
		res.send(html);
		res.end();
	}).catch(function(err){
		console.log(err);
		res.send(err.message);
		res.end();
	});
});

app.get('/consistir', function(req,res){
	pool.query("delete from inconsistencias")
	.then(function(result){
		return pool.query("select * from consistencias order by consistencia")
	}).then(function(result){
		return Promise.all(
			result.rows.map(function(consistencia){
				return pool.query(
					"INSERT into inconsistencias "+
					"  SELECT '"+consistencia.consistencia+"', id"+
					"    FROM encuesta"+
					"    WHERE NOT ("+consistencia.condicion+")"
				);
			})
		);
	}).then(function(result){
		console.log(result);
		res.send('<pre>OK: ');
		res.end();
	}).catch(function(err){
		console.log(err);
		res.send(err.message);
		res.end();
	});
});

app.get('/listado-inconsistencias', function(req,res){
	var preguntas
	pool.query("select * from preguntas order by orden")
	.then(function(result){
		preguntas=result.rows;
		return pool.query("select * from encuesta e inner join inconsistencias i on e.id = i.id order by e.id")
	}).then(function(result){
		var html=`<style>
.aclaracion{
  font-size: 80%
  margin-left: 30px
}
.numero{
	width: 50px
}
input{
  margin-left: 30px
}
td{
	border: 1px solid green
}
</style>
<h1>Listado de inconsistencias en encuestas</h1>
`+
			"<table><tr><th>consistencia"+
				preguntas.map(function(pregunta){
					return "<th title='"+pregunta.pregunta+"'>"+pregunta.variable;
				}).join('')+
			result.rows.map(function(row){
				return "<tr><td>"+row.consistencia+
				preguntas.map(function(pregunta){
					return "<td>"+row[pregunta.variable];
				}).join('');
			}).join('');
		res.send(html);
		res.end();
	}).catch(function(err){
		console.log(err);
		res.send(err.message);
		res.end();
	});
});
