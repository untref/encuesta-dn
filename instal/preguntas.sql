create table "preguntas" (
 "variable" character varying(19),
 "tipo" character varying(6),
 "orden" integer,
 "pregunta" character varying(71),
 "aclaracion" character varying(75),
 primary key ("variable")
);

insert into "preguntas" ("variable", "tipo", "orden", "pregunta", "aclaracion") values
 ('id'                 , 'numero',  1, 'Por favor ingrese el número del cupón sorteado'                         , 'Puede encontrarlo en el reverso del reverso'                                ),
 ('nombre'             , 'texto' , 10, 'Cuál es su nombre de pila'                                              , 'No necesitamos otro dato personal, le pedimos el nombres solo por pedírselo'),
 ('edad'               , 'numero', 20, 'Cuál es su edad'                                                        , null                                                                         ),
 ('sexo'               , 'numero', 30, 'Indique su sexo'                                                        , '1 varón / 2 mujer / 3 intersexual'                                          ),
 ('edad_primer_t'      , 'numero', 40, 'A qué edad empezó a trabajar'                                           , null                                                                         ),
 ('edad_padre_al_nacer', 'numero', 50, 'Qué edad tenía su padre cuando usted nació'                             , null                                                                         ),
 ('edad_padre_pt'      , 'numero', 60, 'Si sabe diga qué edad tenía su padre cuando consiguió su primer trabajo', null                                                                         ),
 ('edad_madre_al_nacer', 'numero', 70, 'Qué edad tenía su madre cuando usted nació'                             , null                                                                         ),
 ('edad_madre_pt'      , 'numero', 80, 'Si sabe diga qué edad tenía su madre cuando consiguió su primer trabajo', null                                                                         );