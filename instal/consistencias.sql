create table "consistencias" (
 "consistencia" character varying(12),
 "condicion" character varying(39),
 "tipo" character varying(5),
 "texto_falla" character varying(137),
 primary key ("consistencia")
);

insert into "consistencias" ("consistencia", "condicion", "tipo", "texto_falla") values
 ('edad_t_prop' , 'edad_primer_t<=edad'                    , 'error', 'la edad del primer trabajo no puede ser mayor a la edad actual'                                    ),
 ('edad_t_padre', 'edad_padre_pt<=edad+edad_padre_al_nacer', 'error', 'la edad del primer trabajo del padre no puede ser mayor a la edad actual del padre calculada en base a la edad en que nació el sujeto'),
 ('edad_t_madre', 'edad_madre_pt<=edad+edad_madre_al_nacer', 'error', 'la edad del primer trabajo de la madre no puede ser mayor a la edad actual de la madre calculada en base a la edad en que nació el sujeto'),
 ('edad_madre'  , 'edad_madre_al_nacer>=10'                , 'adv'  , 'la edad consignada de la madre para el nacimiento del hijo es baja'                                ),
 ('edad_padre'  , 'edad_padre_al_nacer>=10'                , 'adv'  , 'la edad consignada del padre para el nacimiento del hijo es baja'                                  );